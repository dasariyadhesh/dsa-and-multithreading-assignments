package com.innominds.assignments.thread;

/*Write a JAVA program that creates threads by extending Thread class.First thread
 *  display“Good Morning“every 1 sec,the second thread displays“Hello“every 2 seconds
 *   and the third display“Welcome”every 3 seconds,(Repeat the same by implementing Runnable)
 *   Using Different thread class for different Thread
*/
public class Q1ThreadExtThreadClass {
	public static void main(String[] args) throws InterruptedException {
		MyThread1 thread1 = new MyThread1();
		MyThread2 thread2 = new MyThread2();
		MyThread3 thread3 = new MyThread3();
		thread1.start();
		thread2.start();
		thread3.start();

	}

}

class MyThread1 extends Thread {
	public void run() {
		for (int i = 0; i < 20; i++) {
			try {
				System.out.println("Good Morning");
				Thread.currentThread();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class MyThread2 extends Thread {
	public void run() {
		for (int i = 0; i < 20; i++) {
			try {
				System.out.println("Hello");
				Thread.currentThread();
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class MyThread3 extends Thread {
	public void run() {
		for (int i = 0; i < 20; i++) {
			try {
				System.out.println("Welcome");
				Thread.currentThread();
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}