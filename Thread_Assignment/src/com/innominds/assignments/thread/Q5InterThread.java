package com.innominds.assignments.thread;

public class Q5InterThread {
	public static void main(String[] args) {
		Number num = new Number();
		new Producer(num);
		new Consumer(num);

	}
}

class Number {
	int num;
	boolean valueSet = false;

	public synchronized void put(int num) {
		while (valueSet) {
			try {
				wait();
			} catch (Exception e) {
			}
		}
		System.out.println(Thread.currentThread().getName() + " put : " + num);
		this.num = num;
		valueSet = true;
		notify();
	}

	public synchronized void get() {
		while (!valueSet) {
			try {
				wait();
			} catch (Exception e) {
			}
		}
		System.out.println(Thread.currentThread().getName() + " Get : " + num);
		valueSet = false;
		notify();
	}
}

class Producer implements Runnable {
	Number num;

	public Producer(Number num) {
		this.num = num;
		Thread t = new Thread(this, "Producer");
		t.start();
	}

	@Override
	public void run() {
		int i = 0;
		while (true) {
			num.put(i++);
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}

	}

}

class Consumer implements Runnable {
	Number num;

	public Consumer(Number num) {
		this.num = num;
		Thread t = new Thread(this, "Consumer");
		t.start();
	}

	@Override
	public void run() {
		while (true) {
			num.get();
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
			}
		}

	}

}
