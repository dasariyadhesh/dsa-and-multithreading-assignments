package com.innominds.assignments.thread;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Q6DeserializationClass {

	public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException  {
		Q6DeserializationClass deSerializationDemo = new Q6DeserializationClass();
		deSerializationDemo.readEmployeeObject();
	}

	private void readEmployeeObject() throws IOException, FileNotFoundException, ClassNotFoundException {
		FileInputStream fileInputStream = null;
		ObjectInputStream objectInputStream = null;

		try {
			fileInputStream = new FileInputStream("Employee.txt");
			objectInputStream = new ObjectInputStream(fileInputStream);
			Q6Employee employee = (Q6Employee) objectInputStream.readObject();

			System.out.println("Read employee object from the file.");
			System.out.println("Id  = " + employee.getId());
			System.out.println("Name = " + employee.getName());
			System.out.println("Age = " + employee.getAge());
		} finally {

			if (objectInputStream != null) {
				objectInputStream.close();
			}
		}

	}

}
