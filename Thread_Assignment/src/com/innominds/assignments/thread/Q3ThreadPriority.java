package com.innominds.assignments.thread;

class MyThread extends Thread {
	Thread t;
	String name;
	int priority;
	int count = 0;

	public MyThread(String name, int priority) {
		this.name = name;
		this.priority = priority;
		t = new Thread(this, name);
		t.setPriority(priority);
		System.out.println(t.getName() + " thread Started");
		t.start();

	}

	@Override
	public void run() {
		if (t.getPriority() > 8) {
			try {
				Thread.sleep(5000);
				System.out.println(Thread.currentThread().getName() + " is alive? :" + t.isAlive());
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			while (t.isAlive() && count <=5) {
				System.out.println(t.getName() + "counted to" + count);
				count++;
			}

		}

	}
}

public class Q3ThreadPriority {
	public static void main(String[] args) {
		MyThread t1 = new MyThread("My-Thread-1", 2);
		MyThread t2 = new MyThread("My-Thread-2", 4);
		MyThread t3 = new MyThread("My-Thread-3", 9);
		MyThread t4 = new MyThread("My-Thread-4", 10);
		MyThread t5 = new MyThread("My-Thread-5", 5);
		System.out.println(t1.getName() + " is alive? :" + t1.isAlive());
		System.out.println(t2.getName() + " is alive? :" + t2.isAlive());
		System.out.println(t3.getName() + " is alive? :" + t3.isAlive());
		System.out.println(t4.getName() + " is alive? :" + t4.isAlive());
		System.out.println(t5.getName() + " is alive? :" + t5.isAlive());

	}
}
