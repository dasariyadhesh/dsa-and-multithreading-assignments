package com.innominds.assignments.thread;

public class Q2EvenOddThread {
	static int counter = 1;

	public static void main(String... args) throws InterruptedException {
		int s1 = Integer.parseInt(args[0]);
		int s2 = Integer.parseInt(args[1]);
		String msg1 = args[2];
		String msg2 = args[3];
		int n = Integer.parseInt(args[4]);
		// Using Anonymous class by implementing Runnable Interface
		Thread t1 = new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {
					// logic for implementing odd thread
					while (counter <= n) {
						if (counter % 2 == 1) {
							System.out.println(Thread.currentThread().getName() + " " + counter);
							counter++;
						}
						Thread.sleep(s1);
					}

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				notify();

			}
		}, msg1);
		Thread t2 = new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {
					// logic for implementing odd thread
					while (counter <= n) {
						if (counter % 2 == 0) {
							System.out.println(Thread.currentThread().getName() + " " + counter);
							counter++;
						}
						Thread.sleep(s2);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				notify();
			}
		}, msg2);
		t1.start();
		t2.start();
	}
}
