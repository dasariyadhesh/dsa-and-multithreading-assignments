package com.innominds.assignments.thread;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Q6SerialiZationClass {

	public static void main(String[] args) throws FileNotFoundException, IOException  {
		Q6SerialiZationClass serialization = new Q6SerialiZationClass();
		serialization.writeEmployeeObject();
	}

	private void writeEmployeeObject() throws FileNotFoundException, IOException {
		FileOutputStream fileOutputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream("Employee.txt");
			objectOutputStream = new ObjectOutputStream(fileOutputStream);

			Q6Employee employee = new Q6Employee(10904, "Yadhesh", 22);

			System.out.println("Id  = " + employee.getId());
			System.out.println("Name = " + employee.getName());
			System.out.println("Age = " + employee.getAge());

			objectOutputStream.writeObject(employee);
			System.out.println("Written employee object to the file.");
		} finally {

			if (objectOutputStream != null) {
				objectOutputStream.close();
			}
		}

	}

}
