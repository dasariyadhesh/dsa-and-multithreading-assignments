package com.innominds.assignments.thread;

import java.util.Arrays;
import java.util.Scanner;

public class Q7Sorting {
	// inner class
	class Sorting {
		public String[] dataSorting(String[] data) {
			String temp;
			for (int i = 0; i < data.length; i++) {
				for (int j = i + 1; j < data.length; j++) {
					if (data[i].compareTo(data[j]) > 0) {
						temp = data[i];
						data[i] = data[j];
						data[j] = temp;
					}
				}
			}
			return data;

		}

	}

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Enter no of elements : ");
			int n = sc.nextInt();
			System.out.println("Enter " + n + " elements to sort");
			String[] data = new String[n];
			for (int i = 0; i < n; i++) {
				System.out.print("Element " + (i + 1) + ": ");
				data[i] = sc.next();
			}
			Sorting obj = new Q7Sorting().new Sorting();
			System.out.print("Elements after sorted :");
			System.out.println(Arrays.toString(obj.dataSorting(data)));

		}

	}

}
