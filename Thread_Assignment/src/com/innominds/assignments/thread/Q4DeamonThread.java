package com.innominds.assignments.thread;

class Deamon implements Runnable {

	@Override
	public void run() {
		if (Thread.currentThread().isDaemon()) {
			System.out.println(Thread.currentThread().getName() + " is Daemon Thread");
		} else {
			System.out.println(Thread.currentThread().getName() + " is User Thread");
		}

	}

}

public class Q4DeamonThread {
	public static void main(String[] args) {
		Deamon r = new Deamon();
		Thread t1 = new Thread(r);
		Thread t2 = new Thread(r);
		Thread t3 = new Thread(r);

		t1.setDaemon(true);
		t1.start();
		t2.start();
		try {
			t2.setDaemon(true);
		} catch (IllegalThreadStateException e) {
			System.out.println(t2.getName() + " - We can't set thread state as Daemon after thread Started");

		}
		t3.start();

	}

}
