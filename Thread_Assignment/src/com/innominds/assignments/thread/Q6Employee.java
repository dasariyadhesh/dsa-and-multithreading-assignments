package com.innominds.assignments.thread;

import java.io.Serializable;
//transient keyword is used for variable which are not serializable.
public class Q6Employee implements Serializable {
	private static final long serialVersionUID = 10904;
	private int id;
	private String name;
	private transient int age;

	public Q6Employee(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
