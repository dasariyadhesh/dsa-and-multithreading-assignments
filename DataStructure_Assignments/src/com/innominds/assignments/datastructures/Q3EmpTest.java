package com.innominds.assignments.datastructures;

public class Q3EmpTest {
	public static void main(String[] args) {
		Q3EmployeeDB empDB = new Q3EmployeeDB();
		Q3Employee emp1 = new Q3Employee(1, "Yadhesh", "yadhesh@gmail.com", 'M', 250000);
		Q3Employee emp2 = new Q3Employee(2, "Oviya", "oviya@gmail.com", 'F', 350000);
		Q3Employee emp3 = new Q3Employee(3, "Ashok", "ashhok@gmail.com", 'M', 450000);
		Q3Employee emp4 = new Q3Employee(4, "priya", "priya@gmail.com", 'F', 550000);
		Q3Employee emp5 = new Q3Employee(4, "Ashok", "ashhok@gmail.com", 'M', 450000);
		empDB.addEmployee(emp1);
		empDB.addEmployee(emp2);
		empDB.addEmployee(emp3);
		empDB.addEmployee(emp4);
		empDB.addEmployee(emp5);
		for (Q3Employee emp : empDB.listAll())
			System.out.println(emp.getQ3EmployeeDetails());
		System.out.println();

		empDB.deleteEmployee(4);

		for (Q3Employee emp : empDB.listAll())
			System.out.println(emp.getQ3EmployeeDetails());

		System.out.println();
		System.out.println(empDB.showPaySlip(1));

	}
}
