package com.innominds.assignments.datastructures;

import java.util.Scanner;

public class Q5AirlineReservationSystem {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Q5ReservationSystem obj = new Q5ReservationSystem();
		System.out.print("Enter Seat Capacity : ");
		int n=new Scanner(System.in).nextInt();
		obj.seatCapacity(n);
		obj.start();
	}
}
