package com.innominds.assignments.datastructures;

import java.util.LinkedList;
import java.util.List;

public class Q2EvenNumUsingLinkedList {
	List<Integer> A1 = new LinkedList<>();
	List<Integer> A2 = new LinkedList<>();

	public List<Integer> saveEvenNumbers(int N) {
		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0)
				A1.add(i);
		}
		return A1;

	}

	public List<Integer> printEvenNumbers() {
		for (int num : A1) {
			num = num * 2;
			A2.add(num);
		}

		return A2;
	}

	public int printEvenNumber(int N) {
		if (A1.contains(N))
			return N;
		else
			return 0;
	}

	public static void main(String[] args) {
		Q2EvenNumUsingLinkedList obj = new Q2EvenNumUsingLinkedList();
		System.out.println(obj.saveEvenNumbers(10));
		System.out.println(obj.printEvenNumbers());
		System.out.println(obj.printEvenNumber(10));
		System.out.println(obj.printEvenNumber(1));
	}

}
