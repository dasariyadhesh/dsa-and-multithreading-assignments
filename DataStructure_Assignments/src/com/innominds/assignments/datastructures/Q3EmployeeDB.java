package com.innominds.assignments.datastructures;

import java.util.ArrayList;
import java.util.Iterator;

public class Q3EmployeeDB {
	ArrayList<Q3Employee> employee = new ArrayList<>();

	public boolean addEmployee(Q3Employee e) {
		Iterator<Q3Employee> iterator = employee.iterator();
		while (iterator.hasNext()) {
			Q3Employee emp = iterator.next();
			if (emp.getEmpId() == e.getEmpId()) {
				return false;
			}
		}
		return employee.add(e);
	}

	public boolean deleteEmployee(int empId) {
		Iterator<Q3Employee> iterator = employee.iterator();
		while (iterator.hasNext()) {
			Q3Employee emp = iterator.next();
			if (emp.getEmpId() == empId) {
				iterator.remove();
				return true;
			}
		}
		return false;

	}

	public String showPaySlip(int empId) {
		for (Q3Employee e : employee) {
			if (e.getEmpId() == empId) {
				return "PaySlip Generated : income of " + e.getEmpName() + " is " + e.getEmpSalary();
			} else
				return "Enter Valid Employee Details";
		}
		return null;
	}

	public Q3Employee[] listAll() {
		Q3Employee[] empArray = new Q3Employee[employee.size()];
		for (int i = 0; i < employee.size(); i++) {
			empArray[i] = employee.get(i);
		}
		return empArray;

	}

}
