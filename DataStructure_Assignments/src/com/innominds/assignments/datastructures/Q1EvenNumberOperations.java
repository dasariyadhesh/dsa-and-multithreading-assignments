package com.innominds.assignments.datastructures;

import java.util.ArrayList;
/*Develop a java class with a method saveEvenNumbers(int N) using ArrayList to store even numbers from 2 to N, where N is a integer which is passed as a parameter to the method saveEvenNumbers(). The method should return the ArrayList (A1) created.

In the same class create a method printEvenNumbers()which iterates through the arrayList A1 in step 1, and It should multiply each number with 2 and display it in format 4,8,12….2*N. and add these numbers in a new ArrayList (A2). The new ArrayList (A2) created needs to be returned.

Create a method printEvenNumber(int N) parameter is a number N. This method should search the arrayList (A1) for the existence of the number ‘N’ passed. If exists it should return the Number else return zero.

Hint: Use instance variable for storing the ArrayList A1 and A2.

NOTE: You can test the methods using a main method.
*/

public class Q1EvenNumberOperations {
	ArrayList<Integer> A1 = new ArrayList<>();
	ArrayList<Integer> A2 = new ArrayList<>();

	public ArrayList<Integer> saveEvenNumber(int N) {
		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0)
				A1.add(i);
		}
		return A1;
	}

	public ArrayList<Integer> printEvenNumbers() {
		for (int num : A1) {
			num = num * 2;
			A2.add(num);
		}
		return A2;

	}

	public int printEvenNumber(int N) {
		if (A1.contains(N))
			return N;
		else
			return 0;
	}

	public static void main(String[] args) {
		Q1EvenNumberOperations obj = new Q1EvenNumberOperations();
		System.out.println(obj.saveEvenNumber(10));
		System.out.println(obj.printEvenNumbers());
		System.out.println(obj.printEvenNumber(4));
		System.out.println(obj.printEvenNumber(1));

	}

}
