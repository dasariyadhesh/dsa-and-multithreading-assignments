package com.innominds.assignments.datastructures;

import java.util.Scanner;

public class Q5ReservationSystem {
	Scanner sc=new Scanner(System.in);
	boolean[] seats;
	public boolean[] seatCapacity(int n) {
		seats = new boolean[n];
		return seats;
	}

	public void bookSeat() {
		System.out.println("Please type '1' for “smoking” Please type '2' for “non-smoking” and '0' for quit");
		System.out.print("Enter Class : ");
		String passengerClass = sc.next();

		if (passengerClass.equals("1") || passengerClass.equals("2") || passengerClass.equals("0")) {
			if (passengerClass.equals("1")) {
				smokeClassBooking();
			} else if(passengerClass.equals("2")){
				nonSmokeClassBooking();
			}else {
				System.exit(0);
			}
		} else
			System.out.println("Enter valid Number to Select Class");
		System.out.println();
	}
	public void smokeClassBooking() {
		for (int i = 0; i <seats.length/2; i++) {
			if (seats[i] == false) {
				seats[i] = true;
				System.out.println("Seat Booked Succesfully in Smoking Class seat no." + (i + 1));
				break;
			} else if (seats[(seats.length/2)-1] == true) {
				// If both class are booked
				if (seats[seats.length-1] == true) {
					System.out.println();
					System.out.println("All seats are booked.Next flight leaves in 3 hours");
					System.exit(0);
					break;
				} else {
					System.out.println(
							"Smoking Class seats are Booked. Would you like to opt for Non Smoking class please select 'y' for Yes and 'n' for No");
					String selection = sc.next();
					if (selection.equals("y") || selection.equals("n")) {
						if (selection.equals("y")) {
							nonSmokeClassBooking();
							start();
						} else {
							System.out.println("Next flight leaves in 3 hours");
							System.exit(0);
						}
					} else {
						System.out.println("Choose Valid Option");
					}
				}
			}
		}
		
	}
	public void nonSmokeClassBooking() {
		for (int i =(seats.length/2); i <seats.length; i++) {
			if (seats[i] == false) {
				seats[i] = true;
				System.out.println("Seat Booked Succesfully in NonSmoking Class seat no." + (i + 1));
				break;

			} else if (seats[seats.length-1] == true) {
				if (seats[(seats.length/2)-1] == true) {
					// if both classes are fully booked
					System.out.println();
					System.out.println("All seats are booked.Next flight leaves in 3 hours");
					System.exit(0);
					break;
				} else {
					System.out.println(
							"NonSmoking Class seats are Booked. Would you like to opt for Smoking class please select 'y' for Yes and 'n' for No");
					String selection = sc.next();
					if (selection.equals("y") || selection.equals("n")) {
						if (selection.equals("y")) {
							smokeClassBooking();
							start();
						} else {
							System.out.println("Next flight leaves in 3 hours");
							System.exit(0);
						}
					} else {
						System.out.println("Choose Valid Option");
					}
				}
			}
		}
	}
	public void start() {
		while (true) {
			bookSeat();
		}
	}
}
