package com.innominds.assignments.datastructures;

public class Q3Employee {
	private int empId;
	private String empName;
	private String empEmail;
	private char empGender;
	private float empSalary;

	public int getEmpId() {
		return empId;
	}

	public Q3Employee(int empId, String empName, String empEmail, char empGender, float empSalary) {
			super();
			this.empId = empId;
			this.empName = empName;
			this.empEmail = empEmail;
			this.empGender = empGender;
			this.empSalary = empSalary;
		}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public char getEmpGender() {
		return empGender;
	}

	public void setEmpGender(char empGender) {
		this.empGender = empGender;
	}

	public float getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(float empSalary) {
		this.empSalary = empSalary;
	}

	public String getQ3EmployeeDetails() {
		return "Employee Details" + "ID : " + empId + " Name : " + empName + " Email : " + empEmail + " Gender :"
				+ empGender + " Salary : " + empSalary;

	}
}
